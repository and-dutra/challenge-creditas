package core.customer

import services.sendEmail
import java.util.*


data class Customer(val name: String, val email: String) {
    private val subscriptions = mutableListOf<Subscription>()


    fun addSubscription(name: String, expirationDate: Date) {
        var alreadyHasSubscription = subscriptions.any { it.name == name }
        if (alreadyHasSubscription )
            throw Exception("User already subscribe to ${name}")

        subscriptions.add(Subscription(name, expirationDate))

        sendEmail(email, "Sua assinatura do serviço ${name} foi ativada com sucesso")
    }

    fun getSubscriptions(): MutableList<Subscription> {
        return this.subscriptions;
    }
}