package core.customer

import java.util.*


data class Subscription(val name: String, val expirationDate: Date) {}