package core.order

import core.billing.Payment
import core.billing.PaymentMethod
import core.customer.Customer
import core.product.Product
import core.shipping.Address
import core.shipping.Delivery
import java.util.*

class Order(val customer: Customer, val address: Address) {
     val items = mutableListOf<OrderItem>()
    var closedAt: Date? = null
        private set
    var payment: Payment? = null
        private set
    val totalAmount
        get() = items.sumByDouble { it.total }

    fun addProduct(product: Product, quantity: Int) {
        var productAlreadyAdded = items.any { it.product == product }
        if (productAlreadyAdded)
            throw Exception("The core.product have already been added. Change the amount if you want more.")

        items.add(OrderItem(product, quantity))
    }

    fun pay(method: PaymentMethod) {
        if (payment != null)
            throw Exception("The core.order has already been paid!")

        if (items.count() == 0)
            throw Exception("Empty core.order can not be paid!")

        payment = Payment(this, method)

        close()
    }

    fun delivery() {
        Delivery(this)
        close()
    }

    private fun close() {
        closedAt = Date()
    } }
