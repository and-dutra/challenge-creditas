package core.billing

data class CreditCard(val number: String) : PaymentMethod
