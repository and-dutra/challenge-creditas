package core.product

import core.customer.Customer
import core.shipping.Address
import core.shipping.ShippingLabel


class BookProduct(name: String, price: Double) : Product(name, ProductType.PHYSICAL, price ) {

    override fun delivery(customer: Customer, address: Address, shippingLabel: ShippingLabel) {
        val description = "Product: ${this.name}, Price: ${this.price}, Tax: item isento de impostos conforme disposto na Constituição Art. 150, VI, d."
        shippingLabel.addLabel(description)
    }

}
