package core.product

import core.customer.Customer
import core.shipping.Address
import core.shipping.ShippingLabel

class PhysicalProduct(name: String, price: Double) : Product(name, ProductType.PHYSICAL, price ) {
    override fun delivery(customer: Customer, address: Address, shippingLabel: ShippingLabel) {
        val description = "Product: ${this.name}, Price: ${this.price}"
        shippingLabel.addLabel(description)
    }

}