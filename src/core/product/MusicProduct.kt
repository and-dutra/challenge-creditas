package core.product

import core.order.Voucher
import core.customer.Customer
import core.shipping.Address
import core.shipping.ShippingLabel
import services.sendEmail

class MusicProduct(name: String, price: Double) : Product(name, ProductType.DIGITAL, price ) {
    override fun delivery(customer: Customer, address: Address, shippingLabel: ShippingLabel) {
        val voucher = Voucher("PROMO10", 10.0)

        sendEmail(customer.email, "Você comprou ${name} e ganhou um voucher de ${voucher.value} para sua próxima compra, basta usar o código: ${voucher.code} no seu proximo checkout")
    }
}
