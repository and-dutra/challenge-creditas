package core.product

import core.customer.Customer
import core.shipping.Address
import core.shipping.ShippingLabel
import java.util.*

class DigitalProduct(name: String, price: Double) : Product(name, ProductType.DIGITAL, price ) {

    override fun delivery(customer: Customer, address: Address, shippingLabel: ShippingLabel) {
        val date = Calendar.getInstance()   // 19-01-2018

        date.add(Calendar.DATE, +30)

        customer.addSubscription(
            this.name,
            date.time
        )

        println(customer.getSubscriptions())
    }
}