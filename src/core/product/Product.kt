package core.product

import core.customer.Customer
import core.shipping.Address
import core.shipping.ShippingLabel

abstract class Product(val name: String, val type: ProductType, val price: Double) {
    abstract fun delivery(customer: Customer, address: Address, shippingLabel: ShippingLabel);
}