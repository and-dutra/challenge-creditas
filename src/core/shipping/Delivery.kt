package core.shipping

import core.order.Order

class Delivery(private val order: Order,private val shippingLabel: ShippingLabel = ShippingLabel()) {
    init {
        for(item in this.order.items) {
            item.product.delivery(order.customer, order.address, shippingLabel)
        }
        delivery()
    }

    private fun delivery() {
        val label = shippingLabel.generetaLabel(order.address, order.customer)

        //Aqui chamaria o serviço que imprime a etiqueta e envia os produtos físicos
        println(label)
    }
}