package core.shipping

import core.customer.Customer


class ShippingLabel() {
    val items = mutableListOf<ShippingLabelItem>()

    fun addLabel(description: String) {
        items.add(ShippingLabelItem(description))
    }

    fun generetaLabel(address: Address, customer: Customer): String {
        var response = "-------- ETIQUETA -------- \n" +
                "Endereço: ${address.getFullAddress()} \n" +
                "Destinatario: ${customer.name}";

        for (item in items) {
            response += ("\n${item.description}")
        }

        response += "\n---------- FIM -------------"
        return response;
    }
}