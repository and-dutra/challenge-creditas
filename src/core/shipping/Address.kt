package core.shipping

data class Address(val street: String, val number: String, val city: String, val zipcode: String) {
    fun getFullAddress(): String{
        return "${street}, ${number} - ${city} - ${zipcode}";
    }
};