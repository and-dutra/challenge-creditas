import core.billing.CreditCard
import core.customer.Customer
import core.order.Order
import core.product.BookProduct
import core.product.DigitalProduct
import core.product.MusicProduct
import core.product.PhysicalProduct
import core.shipping.Address

fun main(args : Array<String>) {
    val shirt = PhysicalProduct("Flowred T-Shirt",  35.0)
    val netflix = DigitalProduct("Netflix",  29.90)
    val book = BookProduct("The Hitchhiker's Guide to the Galaxy", 120.0)
    val music = MusicProduct("Stairway to Heaven",  5.00)

    val order = Order(Customer("Andre Dutra", "adoliveira@protonmail.com"), Address("Avenida Paulista", "333", "São Paulo", "01300-000"))

    order.addProduct(shirt, 2)
    order.addProduct(netflix, 1)
    order.addProduct(book, 1);
    order.addProduct(music, 1)

    order.pay(CreditCard("43567890-987654367"))
    order.delivery()
}
